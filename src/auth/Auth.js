import { writeFileSync, readFileSync, existsSync } from "fs";
import axios from "axios";
import jwkToPem from "jwk-to-pem";
import keyCloakBackend from "keycloak-backend";
import gql from "graphql-tag";
import jwt from "jsonwebtoken";
import abs from "../abs/index.js";

const {
	REALM_NAME,
	CLIENT_ID,
	AUTH_SERVER_URL,
	AUTH_SERVER_PORT,
	AUTH_SERVER_PROTOCOL,
	PEM_CERTIFICATE_PATH,
} = process.env;

const realmName = REALM_NAME;
const clientId = CLIENT_ID;
const authServerBaseUrl = AUTH_SERVER_URL;
const authServerPort = AUTH_SERVER_PORT;
const authServerProtocol = AUTH_SERVER_PROTOCOL;
const pemCertificatePath = PEM_CERTIFICATE_PATH;
const authServerUrl = `${authServerProtocol}://${authServerBaseUrl}${authServerPort ? `:${authServerPort}` : ""}`;
const certificateUrl = `${authServerUrl}/auth/realms/`;

const keyCloak = keyCloakBackend({
	realm: realmName,
	client_id: clientId,
	"auth-server-url": authServerUrl,
});

export default class Auth {
	constructor() {
		this.initializeCertificate();
		this.isCertificateReinitialized = false;
	}

	identifyRequest = (requestGql = null) => {
		try {
			const request = gql`${requestGql}`;
			const result = request.definitions.filter (
				(definition) => definition.kind === "OperationDefinition"
			);
			return {
				requestType: result[0]?.operation,
				requestName: result[0]?.selectionSet?.selections[0]?.name?.value,
			};
		} catch (error) {
			abs.throwError.catch(`Context #identifyRequest | message: ${error.message}`);
		}
	};

	initializeCertificate = () => {
		if (!existsSync(pemCertificatePath)) {
			(async () => await this.updateCertificate())();
		}
	};

	getCertificateOfToken = (token) => {
		try {
			if (!existsSync(pemCertificatePath)) {
				(async () => await this.updateCertificate())();
			}
			const pemCertificateStringified = String(readFileSync(pemCertificatePath));
			const pemCertificate = JSON.parse(pemCertificateStringified);
			const decoded = jwt.decode(token, { complete: true });
			const certId = decoded?.header?.kid;
			return pemCertificate[certId];
		} catch (error) {
			abs.throwError.catch(`#Auth #getCertificateOfToken | message: ${error}`);
		}
	};


	user = async (token, method) => {
		try {
			this.isCertificateReinitialized = false;
			const user = await this.verifyToken(token, method);
			if (user) return { user };
		} catch (error) {
			abs.throwError.catch(`#Auth #user | message: ${error}`);
		}
	};

	verifyToken = async (token, method) => {
		try {
			if (!token || !method)
				abs.throwError.authentication(`#verifyToken | message: if (!token || !method)`);

			let tokenVerified = false;

			if (method === "query") {
				tokenVerified = await keyCloak.jwt.verifyOffline(token, this.getCertificateOfToken(token));
			} else {
				tokenVerified = await keyCloak.jwt.verify(token);
			}

			if (tokenVerified?.content?.exp && this.isTokenNotExpired(tokenVerified.content.exp)) {
				this.isCertificateReinitialized = false;
				return {
					id: tokenVerified.content.sub,
					username: tokenVerified.content.preferred_username,
				};
			}

			abs.throwError.authentication(`#verifyToken | message: Token Not Verified`);
		} catch (error) {
			if (error.message === "invalid signature") {
				try {
					if (!this.isCertificateReinitialized) {
						this.isCertificateReinitialized = true;
						await this.updateCertificate();
						return await this.verifyToken(token, method);
					}
				} catch (error) {
					abs.throwError.catch(`#Auth #verifyToken #reinitializeCertificate | message: ${error}`);
				}
			}
			abs.throwError.catch(`#Auth #verifyToken | message: ${error}`);
		}
	};

	updateCertificate = async () => {
		try {
			const url = `${certificateUrl}${realmName}/protocol/openid-connect/certs`;
			const jsonCertificate = await axios.get(url);
			const pemCertificate = {};
			jsonCertificate.data.keys.forEach((key) => {
				const certKey = key.kid;
				pemCertificate[certKey] = jwkToPem(key);
			});
			writeFileSync(pemCertificatePath, JSON.stringify(pemCertificate));
			return true;
		} catch (error) {
			abs.throwError.catch(`#Auth #updateCertificate | message: ${error}`);
		}
	};

	isTokenNotExpired = (expiryDate) => {
		return Date.now() < expiryDate * 1000;
	};
}
