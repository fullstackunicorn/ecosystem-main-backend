const resolvers = {

	Query: {
		fetchElementsPublic: async (_, input, context) => {
			return await context.dataSources.element.fetchPublic(input, context);
		},
		fetchElementsPrivate: async (_, input, context) => {
			return await context.dataSources.element.fetchPrivate(input, context);
		},
	},

	Mutation: {
		createElement: async (_, input, context) => {
			return await context.dataSources.element.create(input, context);
		},
	},
};
export default resolvers;
