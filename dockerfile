FROM node:lts-gallium

WORKDIR /app

RUN apt-get update && apt-get install -y openssl

COPY package.json package.json
COPY yarn.lock yarn.lock
COPY . .

RUN yarn install --production

RUN yarn run generate

ENTRYPOINT [ "yarn" ]

CMD [ "run", "start" ]